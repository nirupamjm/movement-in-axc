
import pygame as pg
import guest
import room as r
import constants as c

pg.init()
pg.display.set_caption(c.ORIGINAL_CAPTION)
SCREEN = pg.display.set_mode(c.SCREEN_SIZE)
font = pg.font.Font('freesansbold.ttf', 32)
text = font.render('Welcome Area', True, c.WHITE, c.GREY)
textRect = text.get_rect()
textRect.center = (550, 350)
names = {0: 'imagenes/Dirk.png', 1: 'imagenes/Alex.png'}
Logo = pg.image.load("imagenes/Logo.png").convert()
Map = pg.image.load("imagenes/AXC.png").convert()
clock = pg.time.Clock()

user = guest.User(c.Start_position, names[0])
game_over = False

while game_over == False:

    for event in pg.event.get():
        if event.type == pg.QUIT:
            game_over = True

    user.handle_event(event)
    SCREEN.fill(c.WHITE)
    # insert room



    def roomh2(x, y):
         count = 1
         while (count<10):
             pg.draw.rect(SCREEN, c.GREY, [x, y, 40, 40])
             pg.draw.rect(SCREEN, c.BLACK, [x, y, 40, 40], 3)
             door1 = pg.draw.line(SCREEN, c.RED, (x, y + 10), (x, y + 30), 3)
             door2 = pg.draw.line(SCREEN, c.RED, (x + 40, y + 10), (x + 40, y + 30), 3)
             if user.rect.colliderect(door1):
                 user.rect.x += 60
             if user.rect.colliderect(door2):
                 user.rect.x -= 60
             count=count+1
             x=x+70
    roomh2(270,120)
    def roomh3(x, y):
         count = 1
         while (count<8):
             pg.draw.rect(SCREEN, c.GREY, [x, y, 40, 40])
             pg.draw.rect(SCREEN, c.BLACK, [x, y, 40, 40], 3)
             door1 = pg.draw.line(SCREEN, c.RED, (x, y + 10), (x, y + 30), 3)
             door2 = pg.draw.line(SCREEN, c.RED, (x + 40, y + 10), (x + 40, y + 30), 3)
             if user.rect.colliderect(door1):
                 user.rect.x += 60
             if user.rect.colliderect(door2):
                 user.rect.x -= 60
             count=count+1
             x=x+70
    roomh3(340,190)


    def roomh7(x, y):
        count = 1
        while (count < 16):
            pg.draw.rect(SCREEN, c.GREY, [x, y, 40, 40])
            pg.draw.rect(SCREEN, c.BLACK, [x, y, 40, 40], 3)
            door1 = pg.draw.line(SCREEN, c.RED, (x, y + 10), (x, y + 30), 3)
            door2 = pg.draw.line(SCREEN, c.RED, (x + 40, y + 10), (x + 40, y + 30), 3)
            if user.rect.colliderect(door1):
                user.rect.x += 60
            if user.rect.colliderect(door2):
                user.rect.x -= 60
            count = count + 1
            x = x + 70


    roomh7(130, 470)


    def roomh8(x, y):
        count = 1
        while (count < 4):
            pg.draw.rect(SCREEN, c.GREY, [x, y, 40, 40])
            pg.draw.rect(SCREEN, c.BLACK, [x, y, 40, 40], 3)
            door1 = pg.draw.line(SCREEN, c.RED, (x, y + 10), (x, y + 30), 3)
            door2 = pg.draw.line(SCREEN, c.RED, (x + 40, y + 10), (x + 40, y + 30), 3)
            if user.rect.colliderect(door1):
                user.rect.x += 60
            if user.rect.colliderect(door2):
                user.rect.x -= 60
            count = count + 1
            x = x + 70
    roomh8(480, 540)


    def roomh10(x,y):
        count = 1
        while (count < 13):
            pg.draw.rect(SCREEN, c.GREY, [x, y, 40, 40])
            pg.draw.rect(SCREEN, c.BLACK, [x, y, 40, 40], 3)
            door1 = pg.draw.line(SCREEN, c.RED, (x , y+10), (x , y+30), 3)
            door2 = pg.draw.line(SCREEN, c.RED, (x+40 , y + 10), (x+40 , y + 30), 3)
            if user.rect.colliderect(door1):
                user.rect.x += 60
            if user.rect.colliderect(door2):
                user.rect.x -= 60
            count = count + 1
            x = x + 70


    roomh10(200, 680)


    def roomv(x, y):
        global user
        count = 1
        while (count < 12):
            pg.draw.rect(SCREEN, c.GREY, [x, y, 40, 40])
            pg.draw.rect(SCREEN, c.BLACK, [x, y, 40, 40], 3)
            door1 =  pg.draw.line(SCREEN, c.RED, (x + 10, y), (x + 30, y), 3)
            door2 = pg.draw.line(SCREEN, c.RED, (x + 10, y+40), (x + 30, y+40), 3)
            if user.rect.colliderect(door1):
                user.rect.y += 60
            if user.rect.colliderect(door2):
                user.rect.y -= 60
            # if user.rect.colliderect(door1):
            #     c.coord_x= x+20
            #     c.coord_y= y+20
            #     user = guest.User(c.Start_position, names[0])
            count = count + 1
            y = y + 70


    roomv(550, 50)






    # # INSERt Text
    # SCREEN.blit(text, textRect)
    # # insert logo
    # SCREEN.blit(Logo, [900, 25])
    # # insert MAP
    # SCREEN.blit(Map, [0, 0])
    # insert user
    SCREEN.blit(user.image, user.rect)



    pg.display.flip()
    clock.tick(20)

pg.quit()