import pygame as pg
#import room
import constants as c

class User(pg.sprite.Sprite):
    def __init__(self, position,name):
        self.sheet = pg.image.load(name)
        self.sheet.set_clip(pg.Rect(0, 640, 30, 30))
        self.image = self.sheet.subsurface(self.sheet.get_clip())
        self.rect = self.image.get_rect()
        self.rect.center = (position)
        self.frame = 0
        self.up_states = { 0: (0,512, 64, 64), 1: (64,512, 64, 64), 2: (128,512, 64, 64), 3: (192,512, 64, 64), 4: (256,512, 64, 64), 5: (320,512, 64, 64), 6: (384,512, 64, 64), 7: (448,512, 64, 64), 8: (512,512, 64, 64)}
        self.left_states = { 0: (0,576, 64, 64), 1: (64,576, 64, 64), 2: (128,576, 64, 64), 3: (192,576, 64, 64), 4: (256,576, 64, 64), 5: (320,576, 64, 64), 6: (384,576, 64, 64), 7: (448,576, 64, 64), 8: (512,576, 64, 64)}
        self.down_states = { 0: (0,640, 64, 64), 1: (64,640, 64, 64), 2: (128,640, 64, 64), 3: (192,640, 64, 64), 4: (256,640, 64, 64), 5: (320,640, 64, 64), 6: (384,640, 64, 64), 7: (448,640, 64, 64), 8: (512,640, 64, 64)}
        self.right_states = { 0: (0,704, 64, 64), 1: (64,704, 64, 64), 2: (128,704, 64, 64), 3: (192,704, 64, 64), 4: (256,704, 64, 64), 5: (320,704, 64, 64), 6: (384,704, 64, 64), 7: (448,704, 64, 64), 8: (512,704, 64, 64)}

    def get_frame(self, frame_set):
        self.frame += 1
        if self.frame > (len(frame_set) - 1):
            self.frame = 0
        return frame_set[self.frame]

    def clip(self, clipped_rect):
        if type(clipped_rect) is dict:
            self.sheet.set_clip(pg.Rect(self.get_frame(clipped_rect)))
        else:
            self.sheet.set_clip(pg.Rect(clipped_rect))
        return clipped_rect

    def update(self, direction):

        if direction == 'left':
            self.clip(self.left_states)
            if self.rect.x <= 228:
                self.rect.x += 3
            elif self.rect.x + self.rect.y <= 228 + 40 + 176:
                self.rect.x +=3
            elif self.rect.x - self.rect.y <= 228 - 40 - 426:
                self.rect.x +=3
            else:
                self.rect.x -=3
        elif  direction == 'right':
            self.clip(self.right_states)
            if self.rect.x >= 821:
                self.rect.x -= 3
            elif self.rect.x - self.rect.y >= 228 - 40 + 426 :
                self.rect.x -=3
            elif self.rect.x + self.rect.y >= 228 + 40 + 426 +570:
                self.rect.x -=3
            else:
                self.rect.x +=3
        elif direction == 'up':
            self.clip(self.up_states)
            if self.rect.y <= 40:
                self.rect.y += 3
            elif self.rect.x + self.rect.y <= 228 + 40 + 176 :
                self.rect.y +=3
            elif self.rect.x - self.rect.y >= 228 - 40 + 426:
                self.rect.y +=3
            else:
                self.rect.y -=3
        elif direction == 'down':
            self.clip(self.down_states)
            if self.rect.y >= 623:
                self.rect.y -= 3
            elif self.rect.x - self.rect.y <= 228 - 40 - 426 :
                self.rect.y -=3
            elif self.rect.x + self.rect.y >= 228 + 40 + 426 +570:
                self.rect.y -=3
            else:
                self.rect.y +=3

        elif direction == 'stand_left':
            self.clip(self.left_states[0])
        elif direction == 'stand_right':
            self.clip(self.right_states[0])
        elif direction == 'stand_up':
            self.clip(self.up_states[0])
        elif direction == 'stand_down':
            self.clip(self.down_states[0])

        self.image = self.sheet.subsurface(self.sheet.get_clip())

    def handle_event(self, event):
        if event.type == pg.QUIT:
            game_over = True

        if event.type == pg.KEYDOWN:
        
            if event.key == pg.K_LEFT:
                self.update('left')
            if event.key == pg.K_RIGHT:
                self.update('right')
            if event.key == pg.K_UP:
                self.update('up')
            if event.key == pg.K_DOWN:
                self.update('down')

        if event.type == pg.KEYUP:

            if event.key == pg.K_LEFT:
                self.update('stand_left')
            if event.key == pg.K_RIGHT:
                self.update('stand_right')
            if event.key == pg.K_UP:
                self.update('stand_up')
            if event.key == pg.K_DOWN:
                self.update('stand_down')
        
