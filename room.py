
SCREEN_HEIGHT = 1000
SCREEN_WIDTH = 1100

SCREEN_SIZE = (SCREEN_WIDTH,SCREEN_HEIGHT)

ORIGINAL_CAPTION = "Wellcome to Allripe"


# Colors 

# 	           R    G    B
GRAY         = (100, 100, 100)
NAVYBLUE     = ( 60,  60, 100)
WHITE        = (255, 255, 255)
RED          = (255,   0,   0)
GREEN        = (  0, 255,   0)
FOREST_GREEN = ( 31, 162,  35)
BLUE         = (  0,   0, 255)
SKY_BLUE     = ( 39, 145, 251)
YELLOW       = (255, 255,   0)
ORANGE       = (255, 128,   0)
PURPLE       = (255,   0, 255)
CYAN         = (  0, 255, 255)
BLACK        = (  0,   0,   0)
NEAR_BLACK   = ( 19,  15,  48)
COMBLUE      = (233, 232, 255)
GOLD         = (255, 215,   0)
GREY 		 = (195, 187, 187)
BROWN 		 = (128,  64,   0) 



# User constante

user_left = 'move left'
user_right = 'move right'
user_up = 'move up'
user_down = 'move down'
user_open = 'open object'
user_contact = 'activar chat'

coord_x = 550
coord_y = 350

Start_position = (coord_x,coord_y)
user_sprite = 'imagen user'
id_user = 'id user'
names = { 0: 'imagenes/Dirk.png', 1: 'imagenes/Alex.png'}

# types of rooms 

room_1 = 'room octogono'
room_2 = 'room rectangle'
room_3 = 'room round'
room_4 = 'room 16 subroom'
room_5 = 'room 10 subroom'
room_6 = 'sub-room-4_5'
room_7 = 'room 4 subroom'
room_8 = 'sub-room 7'

#text in room 
text_room = {}
